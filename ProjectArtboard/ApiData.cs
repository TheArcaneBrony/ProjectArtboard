using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectArtboard
{
    #region Initialisation

    #endregion
    
    #region HTML Generators
    public static class ApiPageInterface {
        public static string ListRooms {
            get {
                string a = "";
                foreach(Room r in ApiData.rooms){
                    a += $"• <a href='/board/{r.ID}'>{r.Name}</a><br>\n";
                }
                return a;
            }
        }
    }
    #endregion

    #region Data storage
    public static class ApiData
    {
        public static List<Room> rooms = new List<Room>() {new Room() {Name="Test", ID="Test"}, new Room() {Name="Dev", ID="Dev"}};
    }
    public class Room {
        public string Name;
        public string ID;
        
    }
    #endregion
}
